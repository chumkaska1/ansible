# The task is to practise using variables and templates
Steps to be done:
***
1. Modify playbook from the previous task into a role
2. Make a template of the nginx config
    variables:
        - server_name == test1.juneway.pro test2.juneway.pro
3. Copy nginx config to the standard place
4. Use this config
    server 
	    {
	    listen 80;
	    listen [::]:80;
	    server_name $variable;
	    root /var/www/$variable;
	    index index.html;
	    try_files $uri /index.html;
	    }
5. All variables should be saved to vars directory of the role
6. At the result we have to nginx config on each server
